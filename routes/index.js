var express = require('express');
var router = express.Router();
var pg = require('pg');
var pghost = process.env.OPENSHIFT_POSTGRESQL_DB_HOST || '127.0.0.1';
var pgport = process.env.OPENSHIFT_POSTGRESQL_DB_PORT || '5432';
var pgpass = process.env.OPENSHIFT_POSTGRESQL_DB_PASSWORD || '';
var pguser = process.env.OPENSHIFT_POSTGRESQL_DB_USERNAME || 'postgres';
var connectionString = process.env.DATABASE_URL || 'postgresql://' + pguser + ':' + pgpass + '@' + pghost + ':' + pgport + '/todo';

var path = require('path');

/* GET home page.
 router.get('/', function(req, res, next) {
 res.render('index', { title: 'Express' });
 });
 */
router.get('/', function (req, res, next) {
    res.sendFile(path.join(__dirname, '../views', 'index.html'));
});

router.get('/api/v1/todos', function (req, res) {
    console.log(path.join(__dirname, '../views', 'index.html'));
    var results = [];

    // Get a Postgres client from the connection pool
    pg.connect(connectionString, function (err, client, done) {

        // SQL Query > Select Data
        var query = client.query("SELECT * FROM items ORDER BY id ASC;");

        // Stream results back one row at a time
        query.on('row', function (row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function () {
            client.end();
            return res.json(results);
        });

        // Handle Errors
        if (err) {
            // var query = client.query('CREATE TABLE items(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
            console.log(err);
        }

    });

});


router.post('/api/v1/todos', function (req, res) {

    var results = [];

    // Grab data from http request
    var data = {text: req.body.text, complete: false};

    // Get a Postgres client from the connection pool
    pg.connect(connectionString, function (err, client, done) {

        // SQL Query > Insert Data
        client.query("INSERT INTO items(text, complete) values($1, $2)", [data.text, data.complete]);

        // SQL Query > Select Data
        var query = client.query("SELECT * FROM items ORDER BY id ASC");

        // Stream results back one row at a time
        query.on('row', function (row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function () {
            client.end();
            return res.json(results);
        });

        // Handle Errors
        if (err) {
            console.log(err);
        }

    });
});

router.put('/api/v1/todos/:todo_id', function (req, res) {

    var results = [];

    // Grab data from the URL parameters
    var id = req.params.todo_id;

    // Grab data from http request
    var data = {text: req.body.text, complete: req.body.complete};

    // Get a Postgres client from the connection pool
    pg.connect(connectionString, function (err, client, done) {

        // SQL Query > Update Data
        client.query("UPDATE items SET text=($1), complete=($2) WHERE id=($3)", [data.text, data.complete, id]);

        // SQL Query > Select Data
        var query = client.query("SELECT * FROM items ORDER BY id ASC");

        // Stream results back one row at a time
        query.on('row', function (row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function () {
            client.end();
            return res.json(results);
        });

        // Handle Errors
        if (err) {
            console.log(err);
        }

    });

});

router.delete('/api/v1/todos/:todo_id', function (req, res) {

    var results = [];

    // Grab data from the URL parameters
    var id = req.params.todo_id;


    // Get a Postgres client from the connection pool
    pg.connect(connectionString, function (err, client, done) {

        // SQL Query > Delete Data
        client.query("DELETE FROM items WHERE id=($1)", [id]);

        // SQL Query > Select Data
        var query = client.query("SELECT * FROM items ORDER BY id ASC");

        // Stream results back one row at a time
        query.on('row', function (row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function () {
            client.end();
            return res.json(results);
        });

        // Handle Errors
        if (err) {
            console.log(err);
        }

    });

});


module.exports = router;
