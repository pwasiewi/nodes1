/**
 * Created by pwas on 08.07.15.
 */

var pg = require('pg');
var pghost = process.env.OPENSHIFT_POSTGRESQL_DB_HOST || '127.0.0.1';
var pgport = process.env.OPENSHIFT_POSTGRESQL_DB_PORT || '5432';
var pgpass = process.env.OPENSHIFT_POSTGRESQL_DB_PASSWORD || '';
var pguser = process.env.OPENSHIFT_POSTGRESQL_DB_USERNAME || 'postgres';
var connectionString = process.env.DATABASE_URL || 'postgresql://' + pguser + ':' + pgpass + '@' + pghost + ':' + pgport + '/todo';
//var connectionString = process.env.DATABASE_URL || 'postgres://postgres:@pghost:pgport/todo';

var client = new pg.Client(connectionString);
client.connect();
var query = client.query('CREATE TABLE items(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
query.on('end', function () {
    client.end();
});
